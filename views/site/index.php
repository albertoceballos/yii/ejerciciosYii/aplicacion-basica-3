<?php
    use yii\helpers\html;
    $this->title = 'Inicio';
?>
<div class="site-index">
<div class="row">
      <?php
        foreach($datos as $registro){
      ?>
  <div class="col-xs-6 col-md-3">
    <a href="#" class="thumbnail">
        <?php
            echo  Html::img("@web/imgs/$registro->nombre",['class'=>'img-responsive']); 
        ?>
    </a>
  </div>
    <?php
        }
    ?>
</div>
